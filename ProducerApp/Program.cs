﻿using Apache.NMS;
using Apache.NMS.Util;

namespace ProducerApp
{
    class ProducerProgram
    {
        private const string URI = "activemq:tcp://localhost:61616";
        private const string DESTINATION = "test.queue";

        static void Main(string[] args)
        {
            /*ConnectionFactory connectionFactory = new ConnectionFactory(URI);
            NmsTemplate template = new NmsTemplate(connectionFactory);
            template.ConvertAndSend(DESTINATION, "Hello from the sender.");*/
            /*Console.WriteLine("Hello, World!");*/

            Uri connecturi = new Uri(URI);

            Console.WriteLine("About to connect to " + connecturi);
            IConnectionFactory factory = new NMSConnectionFactory(connecturi);
            IConnection conn = factory.CreateConnection();

            Console.Write("Enter Message: ");

            // Create a string variable and get user input from the keyboard and store it in the variable
            string pesan = Console.ReadLine();

            conn.Start();
            ISession session = conn.CreateSession();
            IDestination dest = SessionUtil.GetDestination(session, DESTINATION);
            IMessageProducer msgProducer = session.CreateProducer(dest);
            msgProducer.DeliveryMode = MsgDeliveryMode.Persistent;

            // Send a message
            ITextMessage request = session.CreateTextMessage(pesan);
            request.NMSCorrelationID = "abc";
            request.Properties["NMSXGroupID"] = "cheese";
            request.Properties["myHeader"] = "Cheddar";

            msgProducer.Send(request);
            session.Close();
            conn.Stop();

            Console.WriteLine("Success send Message");
        }
    }
}

