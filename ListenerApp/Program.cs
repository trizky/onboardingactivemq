﻿// See https://aka.ms/new-console-template for more information
using Apache.NMS;
using Apache.NMS.Util;
using System.Xml;

namespace ListenerApp
{
    class ListenerProgram
    {
        private const string URI = "activemq:tcp://localhost:61616";
        private const string DESTINATION = "test.queue";

        static void Main(string[] args)
        {
            /*ConnectionFactory connectionFactory = new ConnectionFactory(URI);
            NmsTemplate template = new NmsTemplate(connectionFactory);
            template.ConvertAndSend(DESTINATION, "Hello from the sender.");*/
            /*Console.WriteLine("Hello, World!");*/

            Uri connecturi = new Uri(URI);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            Console.WriteLine("About to connect to " + connecturi);
            IConnectionFactory factory = new NMSConnectionFactory(connecturi);
            IConnection conn = factory.CreateConnection();

            conn.Start();
            ISession session = conn.CreateSession();
            IDestination dest = SessionUtil.GetDestination(session, DESTINATION);

            IMessageConsumer consumer = session.CreateConsumer(dest);
            ITextMessage message = consumer.Receive() as ITextMessage;
            if (message == null)
            {
                Console.WriteLine("No message received!");
            }
            else
            {
                Console.WriteLine("Received message with ID:   " + message.NMSMessageId);
                Console.WriteLine("Received message with text: " + message.Text);

                XmlWriter writer = XmlWriter.Create("message.xml", settings);
                writer.WriteStartDocument();
                writer.WriteStartElement("queue");
                writer.WriteElementString("Id", message.NMSMessageId);
                writer.WriteElementString("Message", message.Text);
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Flush();
                writer.Close();
            }
            session.Close();
            conn.Stop();

        }
    }
}

